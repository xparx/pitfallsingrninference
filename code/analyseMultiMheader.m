% Script for loading data from cluster runs for multiple methods.

methods2use = {'lsco','Glmnet'};
methodsNames = {'LSCO','LASSO'};
methodsColor = 'bg';
nM = length(methods2use);

AllINFO           = zeros(40,5);
allSNRrange       = [];
clsAllSMCCErr     = [];
clsAllSMCCopt     = [];
pureAllSMCCErr    = [];
pureAllSMCCopt    = [];
clsAllSSTErr      = [];
clsAllSSTopt      = [];
pureAllSSTErr     = [];
pureAllSSTopt     = [];
pureAllSMCCstdErr = [];
pureAllSSTstdErr  = [];
clsAllSMCCstdErr  = [];
clsAllSSTstdErr   = [];

for k=1:nM
    method = methods2use{k};
    analyseHeader

    %% SMCC
    clsAllSMCCErr = [clsAllSMCCErr, clsSMCCErr];
    clsAllSMCCopt = [clsAllSMCCopt, clsSMCCopt];
    clsAllSMCCstdErr = [clsAllSMCCstdErr, clsSMCCstdErr];

    pureAllSMCCErr = [pureAllSMCCErr, pureSMCCErr];
    pureAllSMCCopt = [pureAllSMCCopt, pureSMCCopt];
    pureAllSMCCstdErr = [pureAllSMCCstdErr, pureSMCCstdErr];

    %% SST
    clsAllSSTErr = [clsAllSSTErr, clsSSTErr];
    clsAllSSTopt = [clsAllSSTopt, clsSSTopt];
    clsAllSSTstdErr = [clsAllSSTstdErr, clsSSTstdErr];

    pureAllSSTErr = [pureAllSSTErr, pureSSTErr];
    pureAllSSTopt = [pureAllSSTopt, pureSSTopt];
    pureAllSSTstdErr = [pureAllSSTstdErr, pureSSTstdErr];

    allSNRrange = [allSNRrange;SNRrange'];

    AllINFO = AllINFO + INFO;
end

AllINFO = AllINFO/nM;

