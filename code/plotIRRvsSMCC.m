% Plot IRR vs MCC
% have to run the commands:
% 
% method = 'Glmnet'; networkSize = 10; analyseHeader; load '../data/randomN10IRR.mat';
% method = 'Glmnet'; networkSize = 45; compileSingleResultData; load '../data/randomN45IRR.mat';
%

lowkY = find(log10(kY) < 0.5);
highkY = find(log10(kY) > 0.5);

if networkSize == 45
    lowkY = find(log10(kY) < 2);
    highkY = find(log10(kY) > 2);
    measure = 'MCC';
    pureMCCopt = squeeze(mean(optimal.(measure)))';
    TTMP = struct2cell(resultsFiles);
    datasets = TTMP(1,:);
    SNRrange = SNRM(1,:);
end

mIRR = squeeze(mean(irr))';
% pureMCCopt;

% colors = zeros(size(datasets));
colors = zeros(1,5);

colors = ones(size(datasets));
colors(lowkY) = 0.75;
% colors = repmat({'r','b'},1,20)';
% [rgb,ncols] = size(datasets);
% colors = [zeros([ncols,rgb]), zeros([ncols,rgb]), ones([ncols,rgb])];
% colors(lowkY,:) = [ones([ncols/2,rgb]), zeros([ncols/2,rgb]), zeros([ncols/2,rgb])];
F=1;
marker = 'sxv+do*d^ph<>.';

%% ==============================================================

F=F+1;
mDIRR = squeeze(mean(localMinDiffIrr))';
h = figure(F); hold on;
for i=1:length(SNRrange)
    scatter(mDIRR(:,i),pureMCCopt(:,i),25,colors,marker(i))
end
% title('Min regressor diff against LASSO MCC')
xlabel('r_{min}')
ylabel('MCC')
ylim([floor(min(min(min(mDIRR))),0) ,1])
grid on

f1 = @(x) sprintf('SNR = %.4g',x);
SNRstring = cellfun(f1, num2cell(SNRrange), 'UniformOutput', false);
hdl = legend(SNRstring,'Location','SouthEast');

hMarkers = findobj(hdl,'type','patch');
set(hMarkers, 'MarkerEdgeColor','k'); % , 'MarkerFaceColor','k');
hold off

saveas(h,fullfile('../img',['N',num2str(networkSize),method,'SIC-minRegDiff.eps']),'epsc')

%% ==============================================================

F=F+1;
tmpirr = -(irr-1);
mOrigIRR = squeeze(mean(tmpirr))';
h = figure(F); hold on;
for i=1:length(SNRrange)
    scatter(mOrigIRR(:,i),pureMCCopt(:,i),25,colors,marker(i))
end
% title('\eta against LASSO MCC')
% \omega = -sic+1
xlabel('\mu')
ylabel('MCC')
ylim([floor(min(min(min(mDIRR))),0) ,1])
grid on
set(gca,'XScale','log');

f1 = @(x) sprintf('SNR = %.4g',x);
SNRstring = cellfun(f1, num2cell(SNRrange), 'UniformOutput', false);
hdl = legend(SNRstring,'Location','SouthWest');

hMarkers = findobj(hdl,'type','patch');
set(hMarkers, 'MarkerEdgeColor','k'); % , 'MarkerFaceColor','k');
hold off

saveas(h,fullfile('../img',['N',num2str(networkSize),method,'SIC-mu.eps']),'epsc')


% %% ==============================================================

% F=F+1;
% mDIRR = squeeze(mean(normDiffIrr))';
% figure(F), hold on;
% for i=1:length(SNRrange)
%     scatter(mDIRR(:,i),pureMCCopt(:,i),25,colors,marker(i))
% end
% title('norm regressor diff against LASSO MCC')
% xlabel('norm reg diff')
% ylabel('MCC')
% grid on

% f1 = @(x) sprintf('SNR = %.4g',x);
% SNRstring = cellfun(f1, num2cell(SNRrange), 'UniformOutput', false);
% hdl = legend(SNRstring,'Location','Best');

% hMarkers = findobj(hdl,'type','patch');
% set(hMarkers, 'MarkerEdgeColor','k'); % , 'MarkerFaceColor','k');
% hold off

% %% ==============================================================

% figure(F), hold on;
% for i=1:length(SNRrange)
%     scatter(mIRR(:,i),pureMCCopt(:,i),25,colors,marker(i))
% end
% title('sic against LASSO MCC')
% xlabel('sic')
% ylabel('MCC')
% grid on
% % ylim([0 1.1])

% f1 = @(x) sprintf('SNR = %.4g',x);
% SNRstring = cellfun(f1, num2cell(SNRrange), 'UniformOutput', false);
% hdl = legend(SNRstring,'Location','SouthWest');

% hMarkers = findobj(hdl,'type','patch');
% set(hMarkers, 'MarkerEdgeColor','k');
% hold off
