% This file defines and loads data. Should be run before any other
% script to define universal variables when running single data set analysis.
%
% requires:
% networkSize = N;
% networkType = 'random';
%

globalDataDir = '/afs/pdc.kth.se/misc/pdc/volumes/sbc/prj.sbc.sparx.2/Benchmark';
datadir = fullfile(globalDataDir,'data/datasets');
netdir = fullfile(globalDataDir,'data/networks');
noiseData = fullfile(globalDataDir,['data/noise/noiseN',num2str(networkSize),'Realisations.mat']);
load(noiseData);

fulldatadir = fullfile(datadir,['N',num2str(networkSize)]);
fullnetdir = fullfile(netdir,networkType,['N',num2str(networkSize)]);

if ~exist('xtra','var')
    xtra = ''; % Search condition for datasets, (works as ls)
end

d = dir(fullfile(fulldatadir,xtra));

datasets = {d(strmatch('Tjarnberg',{d.name})).name};
