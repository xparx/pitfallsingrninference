A = zeros(10,10);
A(:,1:2) = 1;

B1 = zeros(size(A));
B2 = zeros(size(A));
B3 = zeros(size(A));
B4 = zeros(size(A));
B5 = zeros(size(A));

B1(:,1) = 1;
B2(:,1:3) = 1;
B3(:,[1,3]) = 1;
B4(1:2,1) = 1;
B5(1:8,1) = 1;

B = cat(3,A,B1,B2,B3,B4,B5);

M = tools.NetworkComparison(A,B);