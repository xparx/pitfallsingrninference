% run analysis and plot Lorenz 2009 data set.

% if data not availible run:
% [data,net,netExp,netNIR] = compileLorenz2009dataset()

GoldStandardNetworks = [];
GoldStandardNetworks(:,:,1) = net.A;
GoldStandardNetworks(:,:,2) = netExp.A;
GoldStandardNetworks(:,:,3) = netNIR.A;

methodsToUse = {'LASSO','LSCO','Elastic net','RNI'};
zetavec = logspace(-6,0,100);

fprintf('running methods:\n')

disp('Glmnet')
tic
ALASSO = Methods.Glmnet(data,zetavec);
toc

disp('LSCO')
tic
ALSCO = Methods.lsco(data,zetavec);
toc

disp('Elastic net')
tic
AEN = Methods.elasticNet(data,zetavec);
toc

disp('RNI')
tic
ARNI = Methods.RNI(data,0.05);
toc

% disp('Naive Bolasso')
% tic
% ABolasso = Methods.NaiveBolasso(data,zetavec);
% toc

% disp('Naive scale Bolasso')
% tic
% ABolasso = Methods.Naive05Bolasso(data,zetavec);
% toc


%% lasso
M_literature_LASSO   = tools.NetworkComparison(net,ALASSO);
M_experimental_LASSO = tools.NetworkComparison(netExp,ALASSO);
M_NIR_LASSO          = tools.NetworkComparison(netNIR,ALASSO);

%% LSCO
M_literature_LSCO   = tools.NetworkComparison(net,ALSCO);
M_experimental_LSCO = tools.NetworkComparison(netExp,ALSCO);
M_NIR_LSCO          = tools.NetworkComparison(netNIR,ALSCO);

%% Elastic net
M_literature_EN   = tools.NetworkComparison(net,AEN);
M_experimental_EN = tools.NetworkComparison(netExp,AEN);
M_NIR_EN          = tools.NetworkComparison(netNIR,AEN);

%% RNI
M_literature_RNI   = tools.NetworkComparison(net,ARNI);
M_experimental_RNI = tools.NetworkComparison(netExp,ARNI);
M_NIR_RNI          = tools.NetworkComparison(netNIR,ARNI);

% %% Naive Bolasso
% M_literature_bolasso   = tools.NetworkComparison(net,ABolasso);
% M_experimental_bolasso = tools.NetworkComparison(netExp,ABolasso);
% M_NIR_bolasso          = tools.NetworkComparison(netNIR,ABolasso);


measure = 'MCC';

%% Literature network
literature = [M_literature_LASSO.(measure);
              M_literature_LSCO.(measure);
              M_literature_EN.(measure)];
%               M_literature_bolasso.(measure)];
h = figure();
semilogx(zetavec,[literature;[zeros(size(zetavec))]])
% legend({'LASSO','LSCO','Elastic net','Bolasso','RNI'},'Location','NorthWest')
legend({'LASSO','LSCO','Elastic net','RNI'},'Location','NorthWest')
ylabel(measure)
xlabel('\zeta')
saveas(h,fullfile('../img/Lorenz_literature_performance.eps'),'epsc')


%% Experimental network
experimental = [M_experimental_LASSO.(measure);
                M_experimental_LSCO.(measure);
                M_experimental_EN.(measure)];
%                M_experimental_bolasso.(measure)];
h = figure();
semilogx(zetavec,[experimental;[zeros(size(zetavec))]])
legend({'LASSO','LSCO','Elastic net','RNI'},'Location','NorthWest')
ylabel(measure)
xlabel('\zeta')
saveas(h,fullfile('../img/Lorenz_experimental_performance.eps'),'epsc')


%% Optimal NIR network
NIR = [M_NIR_LASSO.(measure);
       M_NIR_LSCO.(measure);
       M_NIR_EN.(measure)];
%       M_NIR_bolasso.(measure)];
h = figure();
semilogx(zetavec,[NIR;[zeros(size(zetavec))]])
legend({'LASSO','LSCO','Elastic net','RNI'},'Location','NorthWest')
ylabel(measure)
xlabel('\zeta')
saveas(h,fullfile('../img/Lorenz_NIR_performance.eps'),'epsc')


%% Generate table

M_NIR_Gold = tools.NetworkComparison(netNIR,GoldStandardNetworks);

nettype = {'S10','S19','S9'};

[optLiterature, indexLitNet] = max(literature,[],2);
[optExperiment, indexExpNet] = max(experimental,[],2);
[optNIR, indexNIRNet] = max(NIR,[],2);

indexStructure = [indexLitNet, indexExpNet, indexNIRNet];
optimal = [optLiterature, optExperiment, optNIR; 0,0,0; M_NIR_Gold.(measure)];

fid = fopen('../data/Lorenz_optimal_performance.tsv','w');

for j=1:3
    fprintf(fid,'\t');
    fprintf(fid,nettype{j});
end
fprintf(fid,'\n');

for i=1:4
    fprintf(fid,methodsToUse{i});
    for j=1:3
        fprintf(fid,'\t');
        fprintf(fid,'%2.2f',optimal(i,j));
    end
    fprintf(fid,'\n');
end

fprintf(fid,'NIR (S9)');
for j=1:3
    fprintf(fid,'\t');
    fprintf(fid,'%2.2f',optimal(end,j));
end
fprintf(fid,'\n');
    
fclose(fid);


%% Export optimal networks to tsv format, cytoscape friendly
optALASSO = ALASSO(:,:,indexStructure(1,:));
optALSCO = ALSCO(:,:,indexStructure(2,:));
optAEN = AEN(:,:,indexStructure(3,:));
% optABolasso = ABolasso(:,:,indexStructure(4,:));

GeneSpider.export2Cytoscape(optALASSO(:,:,1),net.names,['../data/optA_LASSO_S10']);
GeneSpider.export2Cytoscape(optALASSO(:,:,2),net.names,['../data/optA_LASSO_S19']);
GeneSpider.export2Cytoscape(optALASSO(:,:,3),net.names,['../data/optA_LASSO_S9']);

GeneSpider.export2Cytoscape(optALSCO(:,:,1),net.names,['../data/optA_LSCO_S10']);
GeneSpider.export2Cytoscape(optALSCO(:,:,2),net.names,['../data/optA_LSCO_S19']);
GeneSpider.export2Cytoscape(optALSCO(:,:,3),net.names,['../data/optA_LSCO_S9']);

GeneSpider.export2Cytoscape(optAEN(:,:,1),net.names,['../data/optA_EN_S10']);
GeneSpider.export2Cytoscape(optAEN(:,:,2),net.names,['../data/optA_EN_S19']);
GeneSpider.export2Cytoscape(optAEN(:,:,3),net.names,['../data/optA_EN_S9']);

% GeneSpider.export2Cytoscape(optABolasso(:,:,1),net.names,['../data/optA_Bolasso_S10']);
% GeneSpider.export2Cytoscape(optABolasso(:,:,2),net.names,['../data/optA_Bolasso_S19']);
% GeneSpider.export2Cytoscape(optABolasso(:,:,3),net.names,['../data/optA_Bolasso_S9']);

