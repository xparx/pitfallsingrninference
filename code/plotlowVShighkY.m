% script for showing performance difference for low and high k(Y)
% to run first run:
% 
% analyseMultiMheader
% 

nM = length(methods2use);

lowkY = find(log10(kY) < 0.5);
highkY = find(log10(kY) > 0.5);

lowkINFO = AllINFO(lowkY,:);
highkINFO = AllINFO(highkY,:);

methodsNames{nM+1} = 'RNI';
methodsColor(nM+1) = 'k';
nSNR = length(SNRrange);
MPH = ['A':'E']';

% xtiks = [3:nM+3.4:(nM+3.4)*nSNR];
xtiks = [2:nM+2.4:(nM+2)*nSNR];

%% ==========================================================================
%% Optimal performance
lowkData = pureAllSMCCopt(lowkY,:);
highkData = pureAllSMCCopt(highkY,:);

% Optimal performance for high k(Y)
h = figure('color',[1,1,1]);
boxplot([lowkData,lowkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10,'symbol','+')
ylim([-0.05,1.05])
grid on
set(gca,'XGrid','off')
set(gca,'xtick',xtiks)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['Optimal Performance over SNR with low \kappa(Y)'];
% title(TIT)
xlabel({'SNR',''})

ylabel('SMCC (for LASSO and LSCO); FPEL (for RNI)')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','SouthEast')
% poss = get(gca,'Position');
% set(gca,'Position',[1*poss(1),1.01*poss(2),1*poss(3),1*poss(4)])
% xlabh = get(gca,'XLabel');
% set(xlabh,'Position',get(xlabh,'Position') - [0 1 0])
saveas(h,fullfile('../img/performanceLowkYSNR.eps'),'epsc')


% Optimal performance for high k(Y)
h = figure('color',[1,1,1]);
boxplot([highkData,highkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10,'symbol','+')
ylim([-0.05,1.05])
grid on
set(gca,'XGrid','off')
set(gca,'xtick',xtiks)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['Optimal Performance over SNR with high \kappa(Y)'];
% title(TIT)
xlabel('SNR')

ylabel('SMCC (for LASSO and LSCO); FPEL (for RNI)')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','SouthEast')
% poss = get(gca,'Position');
% set(gca,'Position',[poss(1),poss(2),poss(3),0.7*poss(4)])
% xlabh = get(gca,'XLabel');
% set(xlabh,'Position',get(xlabh,'Position') - [0 10 0])
saveas(h,fullfile('../img/performanceHighkYSNR.eps'),'epsc')

return
%% ==========================================================================
%% Minimum RSS performance
lowkData = pureAllSMCCErr(lowkY,:);
highkData = pureAllSMCCErr(highkY,:);

% Minimum RSS perf for high k(Y)
figure('color',[1,1,1]);
boxplot([lowkData,lowkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
llim = ylim;
ylim([llim(1),1.05])
grid on
grid minor
set(gca,'XGrid','off')
set(gca,'XMinorGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['Minimum RSS Performance over SNR with low \kappa(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','NorthWest')


% Minimum RSS perf for high k(Y)
figure('color',[1,1,1]);
boxplot([highkData,highkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.5,1.05])
grid on
grid minor
set(gca,'XGrid','off')
set(gca,'XMinorGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['Minimum RSS Performance over SNR with high \kappa(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','NorthWest')


%% ==========================================================================
%% Minimum CLS RSS performance
lowkData = clsAllSMCCErr(lowkY,:);
highkData = clsAllSMCCErr(highkY,:);

% Minimum RSS perf for low k(Y)
figure('color',[1,1,1]);
boxplot([lowkData,lowkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.5,1.05])
grid on
grid minor
set(gca,'XGrid','off')
set(gca,'XMinorGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['Minimum CLS RSS Performance over SNR with low \kappa(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','NorthWest')


% Minimum RSS perf for high k(Y)
figure('color',[1,1,1]);
boxplot([highkData,highkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.5,1.05])
grid on
grid minor
set(gca,'XGrid','off')
set(gca,'XMinorGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['Minimum CLS RSS Performance over SNR with high k(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','NorthWest')


%% ==========================================================================
%% Minimum CLS RSS performance plus one STD sparse
lowkData = clsAllSMCCstdErr(lowkY,:);
highkData = clsAllSMCCstdErr(highkY,:);

% Minimum RSS perf for low k(Y)
figure('color',[1,1,1]);
boxplot([lowkData,lowkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.2,1.05])
grid on
set(gca,'XGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['One std from Minimum CLS RSS Performance over SNR with low k(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','NorthWest')


% Minimum RSS perf for high k(Y)
figure('color',[1,1,1]);
boxplot([highkData,highkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.4,1.05])
grid on
set(gca,'XGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['One std from Minimum CLS RSS Performance over SNR with high k(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'FontSize',8,'Location','NorthWest')


%% ==========================================================================
%% Minimum RSS performance plus one STD sparse
lowkData = pureAllSMCCstdErr(lowkY,:);
highkData = pureAllSMCCstdErr(highkY,:);

% Minimum RSS perf for low k(Y)
figure('color',[1,1,1]);
boxplot([lowkData,lowkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.2,1.05])
grid on
set(gca,'XGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['One std from Minimum RSS Performance over SNR with low k(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'Location','NorthWest')


% Minimum RSS perf for high k(Y)
figure('color',[1,1,1]);
boxplot([highkData,highkINFO], {[allSNRrange;SNRrange'] [reshape(repmat(MPH(1:nM+1),1,nSNR)',(nM+1)*nSNR,1)]}, 'color',methodsColor,'factorgap',10)
ylim([-0.4,1.05])
grid on
set(gca,'XGrid','off')
set(gca,'xtick',3:nM+3.4:(nM+3.4)*nSNR)
set(gca,'xticklabel',strread(num2str(SNRrange),'%s'))

TIT = ['One std from Minimum RSS Performance over SNR with high k(Y)'];
title(TIT)
xlabel('SNR')
ylabel('SMCC / info')
legend(findobj(gca,'Tag','Box'), fliplr(methodsNames),'FontSize',8,'Location','NorthWest')
