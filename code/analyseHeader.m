% Script for loading data from cluster runs for a specific method.

resultsDir = '/afs/pdc.kth.se/misc/pdc/volumes/sbc/prj.sbc.sparx.2/Benchmark/results';
networkType = 'random';

header

T = dir(fullfile(resultsDir,method,'pitfalls',num2str(networkSize)));

resultsFiles = T(~[T.isdir]);

MCCopt = [];
SSTopt = [];
SNRresults = [];
INFO = [];

pureMCCopt = [];
pureSSTopt = [];
pureMCCErr = [];
pureSSTErr = [];
clsMCCopt = [];
clsSSTopt = [];
clsMCCErr = [];
clsSSTErr = [];
SNRV = [];
SNRNU = [];
SNRE = [];
meanRSSP = [];
meanRSSCLS = [];

for i=1:length(resultsFiles)
    % disp(fullfile(resultsDir,method,'pitfalls',num2str(networkSize),resultsFiles(i).name))
    load(fullfile(resultsDir,method,'pitfalls',num2str(networkSize),resultsFiles(i).name));
    net = GeneSpider.Network.load(fullnetdir,[AllResults.(method).network,'.mat']);
    data = GeneSpider.Dataset.load(fulldatadir,[AllResults.(method).dataset,'.mat']);

    iaa(i) = cond(net.A);
    kY(i) = cond(data.Y);
    kP(i) = cond(data.P);

    zetavec = AllResults.(method).zetavec;
    SNRrange = AllResults.(method).SNRrange;

    INFO(i,:) = mean(info);
    SNRV(i,:) = SNRv;
    SNRNU(i,:) = mean(SNRnu);
    SNRE(i,:) = mean(SNR);

    for j = 1:length(SNRrange)
        x = (j-1)*100+1:j*100;

        %% Pure
        tmpMCC = mean(M1.MCC(x,:));
        tmpSST = mean(M1.sst(x,:));
        [pureMCCopt(i,j), maxInd] = max(tmpMCC);
        [pureSSTopt(i,j), maxInd]  = max(tmpSST);
        
        %% Min RSS performance
        minErrInd = find(mean(RSSP(x,:,1)) == min(mean(RSSP(x,:,1))));
        meanRSSP(i,:,j) = mean(RSSP(x,:,1));
        pureMCCErr(i,j) = tmpMCC(minErrInd(end));
        pureSSTErr(i,j)  = tmpSST(minErrInd(end));

        %% One std from min PURE        
        stdInd = oneSTDEV2(RSSP(x,:,1),RSSP(x,:,2));
        for k = 1:size(tmpMCC,1)
            tmp1(k) = tmpMCC(k,stdInd(k));
            tmp2(k) = tmpSST(k,stdInd(k));
        end
        pureMCCstdErr(i,j) = mean(tmp1);
        pureSSTstdErr(i,j)  = mean(tmp2);

        %% CLS
        tmpMCC = mean(M1.MCC(x,:));
        tmpSST = mean(M1.sst(x,:));
        [clsMCCopt(i,j), maxInd] = max(tmpMCC);
        [clsSSTopt(i,j), maxInd] = max(tmpSST);

        %% Min CLS RSS performance
        minErrInd = find(mean(RSSCLS(x,:,1)) == min(mean(RSSCLS(x,:,1))));
        meanRSSCLS(i,:,j) = mean(RSSCLS(x,:,1));
        clsMCCErr(i,j) = tmpMCC(minErrInd(end));
        clsSSTErr(i,j)  = tmpSST(minErrInd(end));

        %% One std from min CLS
        stdInd = oneSTDEV2(RSSCLS(x,:,1),RSSCLS(x,:,2));
        for k = 1:size(tmpMCC,1)
            tmp1(k) = tmpMCC(k,stdInd(k));
            tmp2(k) = tmpSST(k,stdInd(k));
        end
        clsMCCstdErr(i,j) = mean(tmp1);
        clsSSTstdErr(i,j)  = mean(tmp2);
    end
end
