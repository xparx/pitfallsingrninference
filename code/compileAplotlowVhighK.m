Methods = {'lsco','RNI','Glmnet','elasticNet'};
MethodNames = {'LSCO','RNI','LASSO','Elastic net'};
c = 'gbrk';
Optimal = tools.NetworkComparison();
minRSS = tools.NetworkComparison();
kappaY = [];
kappaP = [];
kappaA = [];
dolooco = false;
alpha = 0.01;

for i=1:length(Methods)
    method = Methods{i};
    networkSize = 10; networkType = 'random';

    compileResultData

    % optimal = reshape(optimal,resultsFiles*nit,length(range));

    Optimal = stack(Optimal,optimal);
    if dolooco
        if strcmp(method,'RNI')
            minRSS = stack(minRSS,optimal);
        else
            minRSS = stack(minRSS,minrssCLS);
        end
    end

    kappaY = [kappaY; kY];
    kappaP = [kappaP; kP];
    kappaA = [kappaA; iaa];
    SNRm = SNRM;

    clearvars -except Methods i Optimal dolooco kappaY kappaP kappaA SNRm minRSS MethodNames alpha c

end

kappaY = kappaY(1,:);
kappaP = kappaP(1,:);
kappaA = kappaA(1,:);

xlabels = mean(SNRm(:,:,1));

%% Plot pitfalls figures version newer.
measure = 'MCC';

lowkY = find(log10(kappaY) < 0.5);
highkY = find(log10(kappaY) > 0.5);

M = Optimal.(measure);
RSSM = minRSS.(measure);

[nmc,SNRlvls,datas] = size(M);

ndatas = datas/length(Methods);

%% optimal parsing data
lowkData = [];
highkData = [];

for i = 1:ndatas:datas
    tmp = M(:,:,i:i+ndatas-1);

    tmpl = tmp(:,:,lowkY);
    tmph = tmp(:,:,highkY);

    tmp = [];
    for j = 1:length(lowkY)
        tmp = [tmp ; tmpl(:,:,j)];
    end

    lowkData = cat(3,lowkData,tmp);

    tmp = [];
    for j = 1:length(highkY)
        tmp = [tmp ; tmph(:,:,j)];
    end

    highkData = cat(3,highkData,tmp);
end

%% RSS parsing data
if dolooco
    lowkRSSData = [];
    highkRSSData = [];

    for i = 1:ndatas:datas
        tmp = RSSM(:,:,i:i+ndatas-1);

        tmpl = tmp(:,:,lowkY);
        tmph = tmp(:,:,highkY);

        tmp = [];
        for j = 1:length(lowkY)
            tmp = [tmp ; tmpl(:,:,j)];
        end

        lowkRSSData = cat(3,lowkRSSData,tmp);

        tmp = [];
        for j = 1:length(highkY)
            tmp = [tmp ; tmph(:,:,j)];
        end

        highkRSSData = cat(3,highkRSSData,tmp);
    end
end

%% Plot data
[ops,h] = illustrate.boxplot(highkData,'x','SNR','y',measure,'c',c,'legend',MethodNames,'width',0.5,'xticklabel',xlabels);
hold on
x = xlim();
plot(x,[0 0],'k--')
hold off
saveas(h,['../img/highkY_',measure,'.eps'],'epsc')


[ops,h] = illustrate.boxplot(lowkData,'x','SNR','y',measure,'c',c,'legend',MethodNames,'width',0.5,'xticklabel',xlabels);
hold on
x = xlim();
plot(x,[0 0],'k--')
hold off
saveas(h,['../img/lowkY_',measure,'.eps'],'epsc')


if dolooco
    [ops,h] = illustrate.boxplot(highkRSSData,'x','SNR','y',measure,'title','high \kappa(Y)','legend',MethodNames,'width',0.5,'xticklabel',xlabels,'title','high \kappa(Y) min RSS');
    hold on
    x = xlim();
    plot(x,[0 0],'k--')
    hold off
    saveas(h,['../junk/highkY_RSS_',measure,'.eps'],'epsc')


    [ops,h] = illustrate.boxplot(lowkRSSData,'x','SNR','y',measure,'title','high \kappa(Y)','legend',MethodNames,'width',0.5,'xticklabel',xlabels,'title','low \kappa(Y) min RSS');
    hold on
    x = xlim();
    plot(x,[0 0],'k--')
    hold off
    saveas(h,['../junk/lowkY_RSS_',measure,'.eps'],'epsc')
end
