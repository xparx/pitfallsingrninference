function ind = oneSTDEV2(RSS,STDRSS)
% ind = oneSTDE(RSS,STDRSS);
% 
% Calculates one STD from minimum from RSS towards end
%
% RSS: the calculated mean rss profiles, 1 per row.
% STDRSS: calculated std of rss profiles, 1 per row.

% meanRSS = mean(RSS);
% stdRSS = std(RSS);

for j=1:size(RSS,1)
    
    lRSS = RSS(j,:);
    stdRSS = STDRSS(j,:);
    
    miniInd = find(lRSS == min(lRSS,[],2));

    limRSS = lRSS(miniInd(end)) + 1.95*stdRSS(miniInd(end));
    
    %% find std - minimum before crossing large RSS value
    for i=miniInd:size(RSS,2)
        if lRSS(i) < limRSS
            ind(j) = i;
        else
            break
        end
    end
    
end