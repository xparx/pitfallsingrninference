% Script for loading data from cluster runs for a specific method.
%
% networkSize = 10;
% networkType = 'random';
% method = 'lsco';
%

resultsDir = '/afs/pdc.kth.se/misc/pdc/volumes/sbc/prj.sbc.sparx.2/Benchmark/results';

header

T = dir(fullfile(resultsDir,method,num2str(networkSize)));

resultsFiles = T(~[T.isdir]);

if ~exist('dolooco','var')
    dolooco = true;
end

optimal = tools.NetworkComparison();
optMp = tools.NetworkComparison();
minrssPure = tools.NetworkComparison();
minrssCLS = tools.NetworkComparison();
minSTDrssCLS = tools.NetworkComparison();
closestNL = tools.NetworkComparison();
below033NL = tools.NetworkComparison();
below0RSS = tools.NetworkComparison();
SNRV = [];
pureRSS = [];
clsRSS = [];
pureSTDRSS = [];
clsSTDRSS = [];
k = 1;
for i=1:length(resultsFiles)
    load(fullfile(resultsDir,method,num2str(networkSize),resultsFiles(i).name),'allM1','allM2','parameters','time2run','SNR*');
    if parameters.alpha ~= alpha
        continue
    end
    if isfield(parameters,method)
        net = GeneSpider.Network.load(fullnetdir,[parameters.(method).network,'.mat']);
        data = GeneSpider.Dataset.load(fulldatadir,[parameters.(method).dataset,'.mat']);
    else
        net = GeneSpider.Network.load(fullnetdir,[parameters.network,'.mat']);
        data = GeneSpider.Dataset.load(fulldatadir,[parameters.dataset,'.mat']);
    end

    zetavec = parameters.zetavec;
    range = parameters.range;
    variable = parameters.variable;
    nit = parameters.nit;

    tmp = parameters;
    % tmp.alpha = 0.01;

    if size(SNRV,1) > size(SNRv,1)
        disp(['  ',resultsFiles(i).name,' was probably not run properly. Data set ',num2str(i)])
    elseif strcmp(tools.DataHash(parameters),tools.DataHash(tmp))
        Parameters(k) = parameters;

        runtime(k) = time2run;
        iaa(k) = cond(net.A);
        kY(k) = cond(data.Y);
        kP(k) = cond(data.P);
        nP1(k) = sum(logical(data.P(1,:)));
        M(k) = data.M;
        nP(k) = nnz(logical(data.P));
        nL(k) = nnz(logical(net));

        SNRV(:,:,k) = SNRv;
        SNRNU(:,:,k) = SNRnu;
        SNRE(:,:,k) = SNR;
        SNRM(:,:,k) = SNRm;

        optMp = maxmax(allM1);
        optimal = stack(optimal,reshape(optMp,nit,length(range)));

        if parameters.looco & dolooco
            load(fullfile(resultsDir,method,num2str(networkSize),resultsFiles(i).name),'RSSP','RSSCLS');

            % Minimum pure RSS performance
            [RSSPvalues,RSSPminInd] = min(RSSP(:,:,1),[],2);
            loocoMp = reshape(getIndex(allM1,RSSPminInd),nit,length(range));
            minrssPure = stack(minrssPure,loocoMp);
            clear loocoMp

            % Minimum CLS RSS performance
            [RSSCLSvalues,RSSCLSminInd] = min(RSSCLS(:,:,1),[],2);
            loocoMc = reshape(getIndex(allM1,RSSCLSminInd),nit,length(range));
            minrssCLS = stack(minrssCLS,loocoMc);
            clear loocoMc

            % One STD above Minimum CLS RSS performance
            minSTDrssCLS = stack(minSTDrssCLS,reshape(getIndex(allM1,oneSTD(RSSCLS(:,:,1),RSSCLS(:,:,2))),nit,length(range)));

            % Closest to true sparsity
            [links,linkIndex] = min(abs(allM1.nlinks-nL(end)),[],2);
            closestNL = stack(closestNL,reshape(getIndex(allM1,linkIndex),nit,length(range)));

            % Lowest error below specific sparsity 1/3
            tmpRSS = RSSCLS(:,:,1);
            nlinks = allM1.nlinks;
            tmpRSS(find(nlinks > 1/3*net.N^2)) = NaN;
            [RSSPvalues,RSSPminInd] = min(tmpRSS(:,:,1),[],2);
            below033NL = stack(below033NL, reshape(getIndex(allM1,RSSPminInd),nit,length(range)));

            % Last error below specific RSS (A=0)
            tmpRSS = RSSCLS(:,:,1);
            tmpRSS(find(bsxfun(@gt,RSSCLS(:,:,1),RSSCLS(:,end,1)))) = NaN;
            [RSSPvalues,RSSPminInd] = max(tmpRSS(:,:,1),[],2);
            below0RSS = stack(below0RSS, reshape(getIndex(allM1,RSSPminInd),nit,length(range)));

            % stack RSS matrices
            pureRSS = cat(3,pureRSS,RSSP(:,:,1));
            clsRSS = cat(3,clsRSS,RSSCLS(:,:,1));
            pureSTDRSS = cat(3,pureSTDRSS,RSSP(:,:,2));
            clsSTDRSS = cat(3,clsSTDRSS,RSSCLS(:,:,2));

            clear RSSP RSSCLS RSSPvalues RSSPminInd allM1 allM2
        end
        k = k + 1;
    end
end
