function [data,net,netExp,netNIR] = compileLorenz2009dataset()
% compile Lorenz 2009 data set.

load '/afs/pdc.kth.se/misc/pdc/volumes/sbc/prj.sbc.sparx.2/Benchmark/data/uncompiled/Lorenz2009data/Lorenz2009_data_extracted_from_supplemental.mat';

nettype   = 'literature';
creator   = 'Lorenz2009';

net = datastruct.Network(double(Lorenz_S10_Structure),nettype);
names = Lorenz_S2_rowheaders;
net.names = names';
net.created.creator = creator;
net.created.time = datenum(2009,00,00);
setname(net);

nReps = 3;

tmpd = struct([]);

tmpd(1).Y = Lorenz_S2_data;
tmpd(1).E = zeros(size(Lorenz_S2_data));
tmpd(1).sdY = Lorenz_S3_SE*sqrt(nReps);
lambda(1) = mean(Lorenz_S3_SE(:).^2*(nReps));
tmpd(1).cvY = eye(size(Lorenz_S2_data))*lambda(1);

tmpd(1).P = diag(Lorenz_S4_P(1,:));
tmpd(1).F = zeros(size(Lorenz_S2_data));
tmpd(1).sdP = diag(Lorenz_S4_P(2,:))*sqrt(nReps);
lambda(2) = mean(Lorenz_S4_P(2,:).^2*(nReps));
tmpd(1).cvP = eye(size(Lorenz_S2_data))*lambda(2);

tmpd(1).lambda = lambda;

tmpd(1).names = names';

data = datastruct.Dataset(net,tmpd);

populate(data,tmpd);

data.created.creator = creator;
data.created.time = datenum(2009,00,00);
setname(data);


%% Literature + experimental "true" network

nettype   = 'literature+experimental';
creator   = 'Lorenz2009';

netExp = datastruct.Network(double(Lorenz_S19_Structure),nettype);
names = Lorenz_S2_rowheaders;
netExp.names = names';
netExp.created.creator = creator;
netExp.created.time = datenum(2009,00,00);
setname(netExp);


%% NIR optimal network

nettype   = 'NIR+optimal';
creator   = 'Lorenz2009';

netNIR = datastruct.Network(double(Lorenz_S9_Afin),nettype);
names = Lorenz_S2_rowheaders;
netNIR.names = names';
netNIR.created.creator = creator;
netNIR.created.time = datenum(2009,00,00);
setname(netNIR);

% clearvars -except data net netNIR netExp
