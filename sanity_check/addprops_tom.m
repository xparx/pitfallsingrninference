function mes = addprops_tom(mes)
mes.addprop('K');
mes.addprop('TK');
mes.addprop('IAA');
mes.addprop('HL');
mes.addprop('SNR');
mes.addprop('method');
mes.addprop('dataset');
mes.addprop('network');
mes.addprop('auroc');
mes.addprop('SNR_calc');
mes.addprop('lambda');
mes.addprop('time');
