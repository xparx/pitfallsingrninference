seed = uint32(round(pi*1/eps));
rng(seed);

SNRs = [0.01,0.1,1,10,100,1000];
methods2use = {'lsco','Glmnet','RNI','aracne'};
methodsNames = {'LSCO','LASSO','RNICO','ARACNeCO'};
methodsColor = 'gmb';
nM = length(methods2use);
networkSize = 10;
networkType = 'random';

N = 10;
M = 30;
kcut = 2*N;

noise_realizations = randn(N,M,100);

% globalDataDir = '../datasets';
globalDataDir = '../../genespider-manuscript';
datadir = fullfile(globalDataDir,'datasets');
netdir = fullfile(globalDataDir,'networks');
fulldatadir = fullfile(datadir,['N',num2str(networkSize)]);
fullnetdir = fullfile(netdir,networkType,['N',num2str(networkSize)]);

if ~exist('xtra','var')
    xtra = ''; % Search condition for datasets, (works as ls)
end
    
d = dir(fullfile(fulldatadir,xtra));
datasets = {d(strmatch('Tjarnberg',{d.name})).name};

j=1;
for i = 1:length(datasets)
    if strcmp(datasets{i}(end-4:end),'.json')
        dd(j) = datasets(i);
        j = j + 1;
    end
end
datasets = dd;
clear dd

M = analyse.CompareModels();
M = addprops_tom(M);

i = 1;
j = 1;
k = 1;
l = 1;

for j = 1:length(methods2use)

    for i = 1:length(datasets)
        data = datastruct.Dataset.load(fullfile(fulldatadir,datasets{i}));
        nets{i} = data.network;
        if isempty(findstr(networkType,data.network)) | ismember(nets{i},nets(1:i-1))
            continue
        end
        net = datastruct.Network.load(fullfile(fullnetdir,[data.network, '.json']));

        for k = 1:length(SNRs)
            lambda = analyse.Data.scale_lambda_SNR_L(data,SNRs(k));

            for l = 1:10
                tmp(1).E = sqrt(lambda)*noise_realizations(:,:,l);
                tmp(1).lambda = lambda;
                tmp(1).Y = true_response(data) + tmp.E;
                populate(data,tmp)

                tic
                [Aest,zetavec] = Methods.(methods2use{j})(data,'full');
                time = toc;
                
                m = analyse.CompareModels(net,Aest);
                m.zetavec = zetavec;
                mm = maxmax(m,'MCC');
                mm = addprops_tom(mm);

                mm.method = methodsNames(j);
                mm.network = {data.network};
                mm.dataset = {data.dataset};
                mm.K = cond(data.Y);
                mm.TK = cond(data.Y - data.E);
                mm.IAA = cond(net.A);
                if cond(true_response(data)) > kcut
                    mm.HL = {'High'};
                else
                    mm.HL = {'Low'};
                end
                mm.auroc = m.AUROC();
                mm.SNR_calc = data.SNR_L;
                mm.SNR = SNRs(k);
                mm.lambda = data.lambda(1);
                mm.time = time;
                
                M = [M, mm];
            end
        end
    end
end
M.save('sanity_check_results.tsv')
% return
