# %matplotlib
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set(style="ticks")

data = pd.read_table('./sanity_check_results.tsv')

colors = ["amber", "windows blue"]
pal = sns.xkcd_palette(colors)

f, axes = plt.subplots(2, 2, figsize=(14, 14), sharex=True)
sns.despine(left=True)


tmp = data[data['method'] == 'RNICO']
sns.boxplot(x="SNR", y="MCC", hue="HL", data=tmp, palette=pal, ax=axes[0, 0])
axes[0, 0].grid(axis='y')
axes[0, 0].set_title('RNICO')

tmp = data[data['method'] == 'LASSO']
sns.boxplot(x="SNR", y="MCC", hue="HL", data=tmp, palette=pal, ax=axes[0, 1])
axes[0, 1].grid(axis='y')
axes[0, 1].set_title('Glmnet')

tmp = data[data['method'] == 'LSCO']
sns.boxplot(x="SNR", y="MCC", hue="HL", data=tmp, palette=pal, ax=axes[1, 0])
axes[1, 0].grid(axis='y')
axes[1, 0].set_title('LSCO')

tmp = data[data['method'] == 'ARACNeCO']
sns.boxplot(x="SNR", y="MCC", hue="HL", data=tmp, palette=pal, ax=axes[1, 1])
axes[1, 1].grid(axis='y')
axes[1, 1].set_title('ARACNeCO')

f.savefig('./sanity_check_performance.pdf')
f.savefig('./sanity_check_performance.png')

# ========================================================================
colors = ["windows blue", "dusty purple", "faded green", "amber"]
pal = sns.xkcd_palette(colors)


f, axes = plt.subplots(1, 2, figsize=(10, 5), sharex=True)
sns.despine(left=True)

tmp = data[data['HL'] == 'High']
sns.boxplot(x="SNR", y="MCC", hue="method", data=tmp, palette=pal, ax=axes[0])
axes[0].grid(axis='y')
axes[0].set_title('High')

# sns.despine(offset=10, trim=True)

tmp = data[data['HL'] == 'Low']
sns.boxplot(x="SNR", y="MCC", hue="method", data=tmp, palette=pal, ax=axes[1])
axes[1].grid(axis='y')
axes[1].set_title('Low')
# sns.despine(offset=10, trim=True)

f.savefig('./sanity_check_performance_HL.pdf')
f.savefig('./sanity_check_performance_HL.png')
