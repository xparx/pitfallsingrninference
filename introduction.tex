\section{Introduction}
\footnotetext{\dag~Electronic Supplementary Information (ESI) available: [at the publishers website.]. See DOI: 10.1039/b000000x/}

\footnotetext{\textit{$^{a}$~Stockholm Bioinformatics Centre, Science for Life Laboratory, Box 1031, 17121 Solna, Sweden}}
\footnotetext{\textit{$^{b}$~Department of Biochemistry and Biophysics, Stockholm University}}
\footnotetext{\textit{$^{c}$~Department of Immunology, Genetics and Pathology, Uppsala University, Rudbeck laboratory, 75185 Uppsala, Sweden.}}
\footnotetext{\textit{$^{d}$~Science for Life Laboratory, Uppsala University.}}
\footnotetext{\textit{$^{e}$~Swedish eScience Research Center}}
\footnotetext{\ddag~These authors contributed equally}

Gene regulatory network (GRN) inference, also known as reverse
engineering or network reconstruction, is an essential endeavour in
Systems biology. Several studies
\citep{Gardner2003,Lorenz2009,Cantone2009} state
that mRNA transcriptional regulatory networks can be inferred based on
gene expression data obtained from \invivo experiments in which all
genes of interest are systematically perturbed and the resulting
expression changes are measured.
To be biologically realistic, the network needs to be relatively sparsely connected, in other words,
only a fraction of all possible links exist.
The \lasso method \citep{Tibshirani1996} and derivatives of it, all of which use $L_1$-regularisation to induce sparsity, achieve this and have become popular for GRN inference.
Several other modelling techniques exist such as Bayesian \citep{Husmeier2007a,Yu2004},
information theoretic \citep{Faith2007,Zhang2012},
neural networks \citep{Grimaldi2010,Hache2007}, Boolean \citep{Wang2012,Beer2004}
and dynamical systems \citep{Gardner2003,Nelander2008}.
Nonetheless, in this study we focus on $L_1$-regularisation methods, in particular \lasso, Elastic Net~\citep{Zou2005}, and Bolasso\cite{Bach2008}, due to their widespread usage. We show that they fail to infer the correct network even when the data is informative enough for correct inference by other methods.
We also test the methods on the \invivo data collected by \citet{Lorenz2009} for inference of the \textit{Snf1} network in \textit{S. cerevisiae} and relate the result to our simulations on \insilico data with known golden standard networks.

Theoretically, \lasso has been shown to be able to recover the correct network under certain conditions, such as the Strong Irrepresentable Condition (SIC) and Restricted Isometry Property (RIP)~\citep{Zhao2006,Candes2008SPM,Candes2009Ann}.
In a network inference context, these conditions concern the relation among observed vectors of expression changes.
However, even results based on SIC only ensure that the \lasso
estimator\index{LASSO@\lasso}
is sign consistent with a probability that goes to one as the number
of samples goes to infinity. Some of the inferred links could
thus not exist in reality, in particular for the low number of samples
seen in biological data sets.
In real applications, SIC is of little use because it cannot be
calculated without knowing the true network.
Even though performance of $L_1$-regularisation methods has been analysed rather extensively, we have not seen any article reporting that they fail for sufficiently informative data, which we show here.

In a number of cases, when reverse engineering algorithms have been applied to biological networks, believed to have a well understood connectivity, networks with a different connectivity have been obtained.
For instance, \citet{Lorenz2009} reported a mere 62\% sensitivity and 69\% precision with 24\% of the
predicted regulatory interactions having the opposite sign in the
model of the \textit{Snf1} network in \textit{S. cerevisiae}.
Moreover, benchmarking studies, such as the Dialogue for Reverse Engineering Assessments and Methods (DREAM), have shown that GRN inference usually results in a large fraction of false positives, \ie inferred links absent in the true network, and false negatives, \ie missed links present in the true network~\citep{Marbach2012,Stolovitzky2009}.
This has in later years lead researchers to complement expression data with other data types, such as binding data, ChIP-seq, and \apriori information~\citep{greenfield2013,Studham2014}.
Note that we here speak about addition of other data types to guide the inference method and not integration of other data types in the model. In the former case the degree of freedom of the model is kept fixed and the data is intended to constrain model parameters, while the degree of freedom in the later case is increased.
Use of these, so called multi-data-type genomic datasets, makes it harder to asses the performance of inference methods compared to expression data alone. It is in particular harder to know to which degree a link is supported by expression data versus \apriori information.
Even if the complete topology of the network is provided,
\eg from ChIP binding data, the signs (activation/repression)
of the links still need to be inferred.
Addition of other data does not fix the method \perse. We therefore think that awareness of the pitfall of $L_1$-regularisation methods that we report here is more essential than before.

A number of GRN inference benchmark studies
\cite{Bansal2007,Penfold2011,Narendra2011} have been published,
spanning a wide range of methods and data sets. In general, the
conclusion is that although they tend to perform better than random,
all inference methods produce models that are far from correct.  The
dependency on the nature of the data is strong as a method may do well
in one benchmark but poorly in another one.
Selection of the regularisation coefficient, which determines the sparsity of the estimate, is a major issue because it must be correct for the estimated network model to be correct~\citep{Tjarnberg2013}. \citet{Vinh2012} detail the difficulties of benchmarking, especially on small networks, where sparsity cannot be achieved to any larger degree due to the network's small size. They show that methods for inference of GRNs do not construct any good networks with sufficient confidence and that the parameter settings of the algorithms are crucial to find a good estimate of the structure of the network. However, no method for optimising these crucial parameters is given. \citet{Jornsten2011} show that the structural agreement between network models inferred for the same biological system using bootstrapping based on measurements obtained at two different platforms only is good for a narrow range of the regularisation coefficient.
This makes it important to assess how the accuracy of different inference methods depends on data and system properties, which we here do for five methods.

Data sets generated \invivo for gold standard networks are rare for
benchmark purposes
due to a lack of knowledge about the interactions among the genes.
An attempt has been made to create such a gold standard for benchmarking by recording an \invivo data set from a synthetically engineered five gene network in yeast, called IRMA~\citep{Cantone2009}.
\citet{Penfold2011} benchmarked time series algorithms in addition to
steady-state algorithms and evaluated their performance on IRMA.
They found that no methods could retrieve the designed structure of
IRMA from the data. The IRMA network was perturbed by single gene
over-expression to trigger the response of the network and the change in mRNA abundance was
then measured
when the system had reached steady-state,
as well as a time series sampled either every 10 or 20 minutes for up to
5 hours.
For single gene perturbations there is no guarantee that the gene space is sufficiently excited to give informative data, \ie that a sufficient variation in the response of the genes over the experiments is achieved~\citep{Nordling2009}. Another issue with gold standard networks is the definition of a link. The inference method and model formalism has to yield the same type of links as recorded in the gold standard in order for a comparison to be meaningful and fair. The five methods employed here infers so called influences, while gold standard networks typically contain links corresponding to physical binding between molecules~\cite{Bansal2007}.
Simulated data sets are thus still necessary for benchmarking due to the lack of ``real'' data sets that are informative enough for accurate GRN inference and differences in the definition of a link.
It is thus not possible to exhaustively demonstrate the pitfalls of $L_1$-regularisation methods on real data, despite the multitude of data that exist. However, we have applied the studied inference methods to the in vivo data collected by \citet{Lorenz2009}, compared the inferred networks to three networks that can be seen as gold standards, and relate the accuracies to the expected performance in our simulations based on the properties of the data.

In this study, we focus on analysing network and data properties that are important for the accuracy of GRN inference.
In particular, the condition number of the network and response matrices, as well as the Signal to Noise Ratio (SNR), are examined. To this end, we generated a set of linear networks with essential properties similar to real biological GRNs. These were then used to generate both gene expression data sets that have properties similar to published \invivo data and data sets that are informative enough for inference of the correct network. This was done to mimic real data sets, while varying the properties and utilising the advantage of knowing the true network.
We restrict ourselves to linear models, because it is sufficient to demonstrate the presented pitfall of $L_1$-regularisation methods. Considering that the class of linear models is a subset of the class of nonlinear models, awareness of this pitfall is essential also when inferring a nonlinear model.
By identifying easily testable conditions that need to be satisfied for successful GRN inference, we provide guidelines useful for avoiding pitfalls that can cause poor network models.



%%% Local Variables:
%%% TeX-master: "main-manuscript"
%%% ispell-local-dictionary: "en_GB"
%%% End:
