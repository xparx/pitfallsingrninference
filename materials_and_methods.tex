\section{Materials and methods}

\subsection{Network inference algorithms}

Least Absolute Shrinkage and Selection Operator (\lasso) penalises
models with small nonzero
parameters by introducing a $L_1$ penalty term in the objective function which equals the sum of the absolute values of the parameters~\cite{Tibshirani1996}
\begin{equation}
  \hat{\mA}_{\textrm{reg}}(\tilde{\zeta}) = \arg \min_{\mA} ||\bA \bY+\bP||_{L_2}^2 + \tilde{\zeta}||\bA||_{L_1} .
  \label{eqn:LASSO}
\end{equation}
The effect of the introduced $L_1$ regularisation term depends on
the regularisation parameter $\zeta$. If it is set to zero then
the ordinary least squares estimate is obtained, while a network
model with no links is obtained when it goes to infinity. The
regularisation term will trade the models predictive performance
on the fitted data for a reduction of the number of descriptive
model parameters.

The Elastic net\cite{Zou2005} is a method based on \lasso which combines the $L_1$ penalty from \lasso and the $L_2$ penalty from ridge regression. The influence of the penalties are then weighted by a parameter $\alpha$ such that,
\begin{equation}
  \hat{\mA}_{\textrm{reg}}(\tilde{\zeta}) = \arg \min_{\mA} C + \tilde{\zeta}\left(\alpha ||\bA||_{L_1} + (1-\alpha)||\bA||_{L_2}^2\right),
  \label{eqn:elastic-net}
\end{equation}
where $C=||\bA \bY+\bP||_{L_2}^2$.

Bolasso\cite{Bach2008} is a bootstrap approach to \lasso inference,
where the statistical properties of bootstrapping are combined with
the \lasso, see algorithm \ref{alg:bootstrap}.
We use a constant number of bootstraps, $n_{BS}=100$, for each data
set, as the statistical confidence should increase with $n_{BS}$. This is well above the minimum number of bootstraps needed\cite{Bach2008}, $\sqrt{N}$,
with $N$ being the number of variables.
\begin{algorithm}
  \caption{Plain bootstrap \lasso algorithm. $\bB$ is the inferred
    model and $\bA$ the logical intersection of inferred
    models. $a_{ij}$ is a link from $j$ to $i$. $n_{BS}$ is the number of
    bootstraps.}
  \label{alg:bootstrap}
  \begin{algorithmic}
    \Procedure{bootstrap lasso}{data,$n_{BS}$}
    \State $a_{ij} = 1~ \forall~ i~ \text{and}~ j~ \in \bA$
    \For{1:$n_{BS}$}
    \State data$_{\text{BS}}$ = \Call{draw with replacement}{data}
    \State $\bB$ = \Call{lasso}{bdata$_{\text{BS}}$,$\zeta$}
    \State $\bA$ = $\bA~ \wedge$ \Call{logical}{$\bB$}
    \EndFor
    \State $\bA = \{a_{ij} \in \bA\}$
    \EndProcedure

    \Function{draw with replacement}{data}
    \State Draw samples with replacement
    \State s.t. $|$data$_{\text{BS}}|$ = $|$data$|$
    \State \Return data$_{\text{BS}}$
    \EndFunction
  \end{algorithmic}
\end{algorithm}
We extend the bootstrap algorithm by requiring that the
bootstrapped data set has the same rank as the original data. In
practice this means putting a rank requirement on the $\mP$ matrix
so that it has full row rank.
This improves the performance, because it ensures that all genes are perturbed in at least one experiment, which is a necessary condition for correct inference~\citep{Nordling2013phdthesis}.
Bolasso was not applied to the 10 gene data sets because the data
matrix becomes rank deficient if a sample that is left out during
the bootstrap procedure contains the only perturbation of a
gene. This is often the case in the 10 gene data, and the
consequence is that links can not achieve $100\%$ bootstrap
support if they can only be inferred when that unique experiment
is sampled. For the same reason, Bolasso was not applied to the
data by \citet{Lorenz2009} as it only consists of one set of single gene
perturbations, leading to a rank deficient data matrix as soon as
one of the experiments is excluded during the bootstrap procedure.

Least-Squares Cut-Off (LSCO) is a simple inference algorithm based on
ordinary least squares (OLS) followed by the removal of all weak links, \ie
small nonzero parameters,
\begin{equation}
  \hat{a}_{ij} \triangleq \left\{
    \begin{array}{ll}  a_{ij}^{ols} & \textrm{if}\; |a_{ij}^{ols}| \geq \tilde{\zeta}\\
      0 & \textrm{otherwise}
    \end{array} \right. \; \textrm{with} \; \mA_{ols} \triangleq
  -\bP\bY^{\dagger} .
  \label{eqn:LSCO}
\end{equation}
The cutoff is used like a sparsity parameter and is varied over a range; for each data set the value producing the network with structure closest to the true network was picked \cite{Tjarnberg2013}.

Robust Network Inference (RNI) is achieved by implicitly checking
all network models that cannot be rejected based on the assumed data model and desired significance level and only including the links that are present in all of these models~\citep{Nordling2013phdthesis}. This gives the intersection of all non-rejectable models. In practice, the network model is obtained by calculating Nordling's confidence score and only including links with a value above one.
Nordling's confidence score for the existence of the link $a_{ij}$ is defined as
\begin{subequations}\label{eqn:ConfidenceScoreSVDscallednormallydistributed}
  \begin{align}
    \gamma(a_{ij}) \triangleq & ~\sigma_{\breve{N}}\left(
      \mPsi(\chi)\right),\quad \\
    \text{with each element}\nonumber\\
    \psi_{kl}(\chi) \triangleq & ~\frac{\psi_{kl}}{\sqrt{\chi^{-2}(\alpha,\breve{N}\breve{M}) \lambda_{kl}}} \\
    \text{and}\; \mPsi \triangleq & ~[\bphi_{1},\ldots,\bphi_{j-1},\bphi_{j+1},\ldots,\bphi_{\breve{N}}, \bxi_i],
  \end{align}
\end{subequations}
assuming that the data has been generated by the data model \eqref{eqn:MatrixEquationWithErrorsConvertedToRegressors},
with $\bupsilon_{j}$ and $\bepsilon_{i}$ drawn from a normal distribution with zero mean and a diagonal covariance matrix \cite{Nordling2013phdthesis}.
Here $\sigma_{\breve{N}}$ denotes the $\breve{N}$th singular value,
and $\invchisquare(\alpha,\breve{N} \breve{M})$ the inverse of the
chi-square cumulative distribution\index{chi-square distribution} with
$\breve{N} \breve{M}$ degrees of freedom, such that
$\Prob[\chisquare(\breve{N} \breve{M}) >
\invchisquare(\alpha,\breve{N} \breve{M})] = \alpha$~\citep{Chew1966}. %, see\eg~\citet{Chew1966}.
A confidence score above one implies that the
link can be proven to exist at the desired significance level
$\alpha$, in this article set to $0.01$.  RNI obtains a network model that under
the assumptions above only contains true
positives~\citep{Nordling2013phdthesis}. False positives are thus
avoided at the expense of accepting false negatives.  RNI was done
using code provided by Nordron AB (\url{www.nordron.com}), which owns all rights.


\subsection{Networks}

To assess the performance of the inference methods we generated a
number of networks by varying model properties that have been considered important in the literature ~\citep{Alter2000,
Tegner2007,
Nordling2009,
Wu2010}.
The sparsity of the networks was set to $0.25$ for $N=10$ based on reported sparsities. For instance, the data on the ten gene network of the {Snf1} signalling pathway in \textit{S.\ cerevisiae}\index{Scerevisiae@\yeast}~\citep{Lorenz2009} can be explained well with networks having a sparsity in the range $0.22$ to $0.28$ and 29 transcriptional regulatory influences have been reported for it in the literature. For $N=45$ we generated networks with a sparsity around $0.07$. Sparsity is
defined as the fraction of links present in the network, denoted $L$, relative to the total number
of possible links, $N^2$, \ie $s \triangleq \frac{L}{N^2}$.
The interampatteness degree for a linear system is defined as
the condition number of the system matrix $\mG = \mA^{-1}$~\citep{Nordling2009}. It is thus $\kappa(\mG) \triangleq  \kappa(\mA) \triangleq \frac{\sigma_1(\mA)}{\sigma_N(\mA)}$, where $\sigma_1(\mA)$ and $\sigma_N(\mA)$ are the largest and smallest singular values of the
network matrix $\mA$, respectively, for each network.
We picked a small value between $\kappa \in [0.5,1]\cdot N$, and a
large value $\kappa \in [9,11]\cdot N$, with $10$ networks for each
level. The latter is within the range reported for real networks based on data on a ten gene network of the {Snf1} signalling pathway in \textit{S.\ cerevisiae}\index{Scerevisiae@\yeast} $253$, and a nine gene subnetwork of the {SOS} pathway in \textit{E.\ coli}\index{Ecoli@\coli} $54$~\citep{Lorenz2009,Gardner2003,Nordling2009}. We generated the networks randomly, while making sure the networks have
full rank, and weighted the model parameters to ensure stability \citep{Zavlanos2011}, and that we achieved the desired $\kappa(\mA)$.

\tabref{tab:network-properties-10} and \tabref{tab:network-properties-45} gives an overview of the $N=10$ and $N=45$ network properties respectively. For a complete list of the networks and properties see
\tabref{tab:10-networks} and \tabref{tab:45-networks}.

\begin{table}[htb]
  \caption{Network properties, for $N=10$ networks}
  \centering
  \resizebox{\linewidth}{!}{%
  \begin{tabular}{lll}
    Network properties                     & low $\kappa(\mA)$ & high $\kappa(\mA)$ \\
    \hline
    \# genes, $N$                          & 10                & 10                 \\
    \# networks                            & 10                & 10                 \\
    structure                              & random            & random             \\
    interampatteness degree, $\kappa(\mA)$ & $6.9-10$        & $91.6-108$     \\
    sparsity                               & $0.25$            & $0.25$             \\
  \end{tabular}}
  \label{tab:network-properties-10}
\end{table}

\begin{table}[htb]
  \caption{Network properties, for $N=45$ networks}
  \centering
  \resizebox{\linewidth}{!}{%
  \begin{tabular}{lll}
    Network properties                     & low $\kappa(\mA)$ & high $\kappa(\mA)$ \\
    \hline
    \# genes, $N$                          & 45                & 45                 \\
    \# networks                            & 10                & 10                 \\
    structure                              & random            & random             \\
    interampatteness degree, $\kappa(\mA)$ & $25.4-41.3$       & $411.5-492.8$      \\
    sparsity                               & $\approx0.07$     & $\approx0.07$      \\
  \end{tabular}}
  \label{tab:network-properties-45}
\end{table}

\subsection{Data sets}

Data sets were created according to the linear dynamical model in
\eqref{eq:Linearmap}. Given the true network $\check{\mA}$ we calculate
an initial $\mP$ generated with the given perturbation design. Our
noiseless expression data is then $\check{\mY} = -\check{\mA}^{-1}\mP$.
We included $2N$ samples for $N=10$ and $4N$ samples for $N=45$, because published data sets typically contain one to three replicates of $N$ experiments~\citep{Gardner2003,Lorenz2009,Cantone2009}.

We followed three different perturbation approaches two for $N=10$: Naive Random Double
Perturbation (NRDP) and Sparse Balanced Excitation Design (SBED), and one for $N=45$: triple Single Sets and a Single Double set (SSSD).

NRDP was constructed by perturbing two
randomly chosen genes for each sample while making sure that $\bP$ had full rank and that
each gene was perturbed at least once.
By perturbing genes more than once we make sure that each sample has some
dependency on the remaining data set, a requirement for using the
sample in leave one out cross-optimisation of $\zeta$~\citep{Tjarnberg2013}.
This design yields data sets where the condition number of $\mY$ is close to the interampatteness degree of the network. We therefore generate data sets with similar conditions to those reported in the literature, $5$, $154$, and $215$, respectively in \citet{Alter2000,Gardner2003}, and \citet{Lorenz2009}.

The objective of the SBED is to excite all directions of the gene space uniformly, \ie spread out the response equally in the gene space, and obtain a well conditioned $\mY$ matrix~\cite{Nordling2009}.
We do this approximately by minimising $\kappa(\bY)$ and the number of perturbed genes.
To achieve uniform excitation is simpler for a dense perturbation matrix $\mP$ as the different signal directions in $\mY$ can be more easily tuned.
However as the possibility to perturb a majority of the genes at once is unrealistic, we keep $\mP$ as sparse as possible, \ie we do a trade-off between a sparse perturbation design and uniform excitation in all directions of the gene space.

The SSSD perturbation design is constructed by using triple replicates where a single gene is perturbed for each sample with one extra set of double perturbation where two random genes are perturbed for each sample. This setup simulates a plausible experimental design approach that naively tries to maximise the information in the data set while utilising the fact that there need to be a dependence between samples to do some form of cross validation.

\tabref{tab:10-dataset-properties} and \tabref{tab:45-dataset-properties} shows an overview of data set
properties.  For a complete list of the data sets and properties see
\tabref{tab:SBED-datasets}, \tabref{tab:NRDP-datasets}, and \tabref{tab:45-datasets}.

\begin{table}[htb]
  \caption{Data set properties}
  \centering
  \resizebox{\linewidth}{!}{%
  \begin{tabular}{lll}
    Data set property               &           &             \\
    \hline
    perturbation design             & SBED      & NRDP        \\
    samples, $M$                    & $2N$      & $2N$        \\
    \# data sets                    & $20$      & $20$        \\
    condition number, $\kappa(\mY)$ & $1.3-2.0$ & $9.5-181.3$ \\
    max \# perturbations per sample & $2-6$     & $2$         \\
    min \# perturbations per sample & $1-3$     & $2$         \\
  \end{tabular}}
  \label{tab:10-dataset-properties}
\end{table}

\begin{table}[htb]
  \caption{Data set properties}
  \centering
  \resizebox{\linewidth}{!}{%
  \begin{tabular}{lll}
    Data set property               &             &                \\
    \hline
    perturbation design             & SSSD        & SSSD           \\
    samples, $M$                    & $4N$        & $4N$           \\
    \# data sets                    & $10$        & $10$           \\
    condition number, $\kappa(\mY)$ & $25.7-41.3$ & $412.8-504.51$ \\
    max \# perturbations per sample & $2$         & $2$            \\
    min \# perturbations per sample & $1$         & $1$            \\
  \end{tabular}}
  \label{tab:45-dataset-properties}
\end{table}

We applied noise to each data set with a variance $\lambda$ selected to give the desired Signal to Noise Ratio (SNR)
\begin{equation}
  \text{SNR} \triangleq \frac{\sigma_N(\check{\mPhi})}{\sqrt{\chi^{-2}(\alpha,NM)\lambda}},
  \label{eq:SNR}
\end{equation}
where $\sigma_N(\check{\mPhi})$ is the $N$th singular value of $\check{\mPhi}$,
and $\chi^{-2}(\alpha,NM)$ is the inverse of the chi-square cumulative
distribution function as explained above.
We generated $100$ different noise realisations to do Monte Carlo simulations, each from a normal distribution with zero mean and variance $\lambda$ using the {\tt randn} function in
Matlab version R2012a (\url{www.mathworks.com}).
For each data set, the variance of each realisation was then scaled based on \eqref{eq:SNR} to achieve the desired SNR.
For the data sets we used the significance level $\alpha = 0.01$.
By covering the whole range of SNRs from completely uninformative to informative enough, we include the levels seen in real data.

\subsection{Performance evaluation}

We assessed the accuracy of the estimated networks using the Matthew
Correlation Coefficient (MCC)~\cite{Baldi2000}.
MCC accounts for both true positives (TP), false positives (FP), true negatives (TN), and false negatives (FN), providing one number in the range $[-1,1]$ that captures the structural similarity between two networks containing the same labelled nodes. To use it one needs a golden standard that is taken as the true network that all the estimates are compared to.

The Fraction of Provably Existing Links (FPEL) is the fraction of links existing in the true network that can be proven to exist based on the observed data. They are proven to exist by rejecting all alternative network models lacking these links at a desired significance level based on the observed data and true data model, \ie when the considered set of network models contains the true network and the measurement noise is described by the error model that was used to generate it. FPEL is calculated as the number of links with Nordling's confidence score \eqref{eqn:ConfidenceScoreSVDscallednormallydistributed} above one divided by the number of links in the true network~\citep{Nordling2013phdthesis}.
It is the sensitivity of RNI. If all existing links can be proven to exist, \ie FPEL $=1$, then the data set is said to be informative enough for network inference. Note that FPEL and MCC are not directly comparable, since only the later accounts for FP and TN. MCC is relative to the number of possible links $N^2$, while FPEL is relative to the number of links present in the true network $L$. Nonetheless, MCC $=1$ corresponds to FPEL $=1$ for RNI.
Only measurement data and an error model is needed to calculate the number of provably existing links, implying that it can be used for validation even when no golden standard or true network exists.

\subsection{Analysis of the irrepresentable condition}

The network model in \eqref{eqn:MatrixEquationWithErrorsConvertedToRegressorsSystem} can for each row $i$ of the interaction matrix $\mA$ be expressed as $\check{\mPhi} \check{\btheta}_i = \check{\bxi}_i$, yielding a sparse estimation problem for each row.
By introducing $\mPhi_{0_i}$ and $\mPhi_{0_i^c}$ that contain regressors corresponding to the zero and nonzero elements of $\check{\btheta}_i$, respectively, and $\bbeta_i$ containing the nonzero elements of $\check{\btheta}_i$, the Common part of the Irrepresentable Conditions (CIC), used by \citet{Zhao2006} in theorems ensuring sign consistency of the \lasso estimator, can be expressed as
\begin{align}
  \tilde{\bmu}_i \triangleq \abs{\mPhi_{0_i}^T \mPhi_{0_i^c} \left( \mPhi_{0_i^c}^T \mPhi_{0_i^c} \right)^{-1} \sign(\bbeta_i)} .
  \label{eqn:CIC}
\end{align}

If all elements of $\tilde{\bmu}_i$ are smaller than 1, then the Weak Irrepresentable Condition (WIC) is fulfilled and if all elements are smaller than 1 minus a positive constant $\eta$, then the Strong Irrepresentable Condition (SIC) is fulfilled~\citep{Zhao2006}. The latter is used to show that \lasso is strongly sign consistent and the former that it is general sign consistent; both imply that the probability that all elements in the \lasso estimate of $\btheta_i$ have the correct sign goes to one when the number of samples $\breve{M}$ goes to infinity.
A few additional technical conditions are required in the theorems, but it is logical to expect a high accuracy of the network estimate produced by \lasso when
\begin{equation}
  \mu \triangleq \max_i \max \tilde{\bmu}_i < 1
  \label{eqn:MaxCIC}
\end{equation}

If all columns in $\mPhi_{0_i}$ are orthogonal to all columns in $\mPhi_{0_i^c}$, then $\tilde{\bmu}_i = 0$ and SIC is fulfilled.
Assume for a moment that $\mPhi_{0_i} = \bphi_1$ and $\mPhi_{0_i^c} = \bphi_2$, then $\tilde{\bmu}_i = \abs{\bphi_1^T \bphi_2 \norm{\bphi_2}^{-2} }$. Now if $\bphi_1 = \alpha \bphi_2$, \ie $\bphi_1$ is parallel to $\bphi_2$, then $\tilde{\bmu}_i = \abs{\alpha}$ is greater or equal to one unless $\alpha$ is smaller than one, \ie unless $\bphi_1$ is shorter than $\bphi_2$.
Hence the projection of any regressor corresponding to a zero element that is not orthogonal to the regressors corresponding to a nonzero element onto them must always be shorter than all of them to fulfill SIC. This would always hold if all regressors corresponding to a zero were shorter than all regressors corresponding to a nonzero element.
This makes it interesting to calculate the minimum ratio between the shortest regressor corresponding to a nonzero element and the longest corresponding to a zero over all rows
\begin{equation}
  r_{\min} \triangleq \min_i \left(
    \frac{\min_{\bphi_k \in \mPhi_{0_i^c} } \norm{\bphi_k} }{\max_{\bphi_l \in \mPhi_{0_i} } \norm{\bphi_l} }\right) .
  \label{eqn:MinDiffReg}
\end{equation}


%%% Local Variables:
%%% TeX-master: "main-manuscript"
%%% ispell-local-dictionary: "en_GB"
%%% End:
