\renewcommand{\thesection}{S.\arabic{section}}
\renewcommand{\thesubsection}{\thesection.\arabic{subsection}}
\renewcommand{\thefigure}{S.\arabic{figure}}
\renewcommand{\thetable}{S.\arabic{table}}
\renewcommand{\thealgorithm}{S.\arabic{algorithm}}


% Hack for making SOM Equations Conform to Science Format
%
% e.g. (S1), (S2), etc
% Requires AMS
\makeatletter %% With ams
\def\tagform@#1{\maketag@@@{(S\ignorespaces#1\unskip\@@italiccorr)}}
\makeatother
\setcounter{page}{1}
\setcounter{figure}{0}
\setcounter{section}{0}
\setcounter{table}{0}
\setcounter{algorithm}{0}

\onecolumn
\section{Supplementary material}

\subsection{Analysis of biological data}

\citet{Lorenz2009} presents the result of running the inference method NIR on their data set in terms of sensitivity = TP/(TP+FN) and precision = TP/(TP+FP), and reports the values $62\%$ and $69\%$, respectively. The corresponding values that we calculate without considering the sign of the interactions for the given matrices in their supplemental data is $0.7\%$ and $0.76\%$, respectively.

To calculate the variance, $\lambda$ of the data matrices in \citealt{Lorenz2009}, we extracted the propagated standard error (SE)\cite{Gardner2003} for each data point as reported in their supplementary table S2 and S3. We then calculated the weighted average variance, 
\begin{equation}
  \lambda = \frac{R}{NM} \sum_{i=1}^N\sum_{j=1}^M \text{SE}_{ij}^{2},
  \label{equ:lambda-mean-lorenz}
\end{equation}
where $R$ is the number of biological replicates, $N$ is the number of genes, $M$ the number of experiments and $\text{SE}_{ij}$ is the propagated standard error for data point $ij$.

\begin{figure}[p!]
  \centering
  \subfigure[\citet{Lorenz2009} S10 network]
  {\label{fig:S10-performance}
    \includegraphics[width=0.45\linewidth]{./img/Lorenz_literature_performance}}
  \subfigure[\citet{Lorenz2009} S19 network]
  {\label{fig:S19-performance}
    \includegraphics[width=0.45\linewidth]{./img/Lorenz_experimental_performance}}
  \subfigure[\citet{Lorenz2009} S9 network]
  {\label{fig:S9-performance}
    \includegraphics[width=0.45\linewidth]{./img/Lorenz_NIR_performance}}
  \caption{Performance over the full regularisation path for \citet{Lorenz2009} networks}
  \label{fig:Lorenz-performance}
\end{figure}


\begin{table}[p!]
  \caption{Properties of data sets with SBED perturbations used in simulations. $\kappa(\mA)$ is the
    interampatteness degree of the network and $\kappa(\mY)$ is the
    condition number of the response matrix. $\# (~p_{ij} \neq 0)$ is
    the total number of perturbations in the matrix $\mP$. The
    $\overline{\bp_{1,0}} = \max_j||\bp_{i} \neq 0||_1$, that is, the maximum number of perturbations/experiment
    over all experiments and $\underline{\bp_{1,0}} = \min_j||\bp_{i}
    \neq 0||_1$ is the corresponding minimum. $\lambda(\SNR = 1)$ is the variance of the noise
    for $\SNR=1$.}
  \centering
  \begin{tabular}{lllllll}
    ID  &  $\kappa(\bA)$ &  $\kappa(\bY)$ & $\# (~p_{ij} \neq 0)$ & $\underline{\bp_{1,0}}$ & $\overline{\bp_{1,0}}$  &  $\lambda(\SNR = 1)$\\
    \hline
    1  & 100.56 & 1.54   & 56 & 2 & 5 &  0.00237 \\
    2  & 107.95 & 1.45   & 45 & 1 & 4 &  0.0025  \\
    3  & 6.90   & 1.76   & 29 & 1 & 3 &  0.00198 \\
    4  & 7.55   & 1.93   & 28 & 1 & 2 &  0.00175 \\
    5  & 7.82   & 1.87   & 28 & 1 & 2 &  0.00169 \\
    6  & 7.89   & 1.98   & 34 & 1 & 3 &  0.00172 \\
    7  & 8.76   & 1.48   & 31 & 1 & 4 &  0.00242 \\
    8  & 9.07   & 1.43   & 38 & 1 & 4 &  0.00264 \\
    9  & 91.61  & 1.49   & 68 & 1 & 5 &  0.00248 \\
    10 & 92.75  & 1.57   & 90 & 3 & 5 &  0.00225 \\
    11 & 92.75  & 1.40   & 50 & 1 & 4 &  0.00265 \\
    12 & 92.87  & 1.33   & 56 & 1 & 6 &  0.00288 \\
    13 & 9.44   & 1.34   & 36 & 1 & 4 &  0.00263 \\
    14 & 94.95  & 1.47   & 33 & 1 & 4 &  0.00257 \\
    15 & 9.54   & 1.80   & 42 & 1 & 6 &  0.00181 \\
    16 & 96.49  & 1.47   & 35 & 1 & 4 &  0.00253 \\
    17 & 96.97  & 1.57   & 56 & 1 & 6 &  0.00221 \\
    18 & 99.00  & 1.81   & 35 & 1 & 3 &  0.00188 \\
    19 & 9.95   & 1.70   & 29 & 1 & 3 &  0.00196 \\
    20 & 9.99   & 1.78   & 39 & 1 & 5 &  0.00178 \\
    \hline
  \end{tabular}
  \label{tab:SBED-datasets}
\end{table}


\begin{table}[p!]
  \caption{Properties of data sets with NRDP experimental design used
    in simulations. $\kappa(\mA)$ is the
    interampatteness degree of the network and $\kappa(\mY)$ is the
    condition number of the response matrix. $\# (~p_{ij} \neq 0)$ is
    the total number of perturbations in the matrix $\mP$. The
    $\overline{\bp_{1,0}} = \max_j|\bp_{i} \neq 0|_1$, that is, the maximum number of perturbations/experiment over all experiments and $\underline{\bp_{1,0}} = \min_j||\bp_{i}
    \neq 0||_1$ is the corresponding minimum. $\lambda(\SNR = 1)$ is the variance of the noise
    for $\SNR=1$.}
  \centering
  \begin{tabular}{lllllll}
    ID  &  $\kappa(\bA)$ &  $\kappa(\bY)$ & $\# (~p_{ij} \neq 0)$ & $\underline{\bp_{1,0}}$ & $\overline{\bp_{1,0}}$  &  $\lambda(\SNR = 1)$\\
    \hline
    1  & 100.56 & 93.69  & 40 & 2 & 2 &  0.000108\\
    2  & 107.95 & 175.47 & 40 & 2 & 2 &  7.72e-05\\
    3  & 6.90   & 9.52   & 40 & 2 & 2 &  0.0187  \\
    4  & 7.55   & 10.56  & 40 & 2 & 2 &  0.0143  \\
    5  & 7.82   & 10.92  & 40 & 2 & 2 &  0.0167  \\
    6  & 7.89   & 11.52  & 40 & 2 & 2 &  0.0129  \\
    7  & 8.76   & 14.22  & 40 & 2 & 2 &  0.0103  \\
    8  & 9.07   & 15.71  & 40 & 2 & 2 &  0.00855 \\
    9  & 91.61  & 131.57 & 40 & 2 & 2 &  5.57e-05\\
    10 & 92.75  & 124.70 & 40 & 2 & 2 &  7.97e-05\\
    11 & 92.75  & 153.94 & 40 & 2 & 2 &  8.4e-05 \\
    12 & 92.87  & 135.91 & 40 & 2 & 2 &  0.000104\\
    13 & 9.44   & 13.52  & 40 & 2 & 2 &  0.00936 \\
    14 & 94.95  & 95.24  & 40 & 2 & 2 &  0.000126\\
    15 & 9.54   & 16.48  & 40 & 2 & 2 &  0.00659 \\
    16 & 96.49  & 99.79  & 40 & 2 & 2 &  0.000111\\
    17 & 96.97  & 181.29 & 40 & 2 & 2 &  5.07e-05\\
    18 & 99.00  & 150.37 & 40 & 2 & 2 &  0.000103\\
    19 & 9.95   & 23.03  & 40 & 2 & 2 &  0.00306 \\
    20 & 9.99   & 11.92  & 40 & 2 & 2 &  0.00771 \\
    \hline
  \end{tabular}
  \label{tab:NRDP-datasets}
\end{table}

\begin{table}[p!]
  \caption{Properties of network models of size N=10. $\kappa(\bA)$ is the
    interampatteness degree of the network, $\#$ SC is the number of
    strongly connected components in the network as reported by {\tt
      graphconncomp} in MATLAB. $\tau(\bG)$ is the time constant for
    the system $\mG = -\mA^{-1}$.}
  \centering
  \begin{tabular}{llllll}
    ID & Type  & $\kappa(\bA)$ &  $\#$ links &  $\#$ SC & $\tau(\bG)$\\
    \hline
    1  &  random &  100.56 &  25 &   8  &  0.11 \\
    2  &  random &  107.95 &  25 &   5  &  0.115\\
    3  &  random &  6.90   &  25 &   4  &  0.186\\
    4  &  random &  7.55   &  25 &   6  &  0.193\\
    5  &  random &  7.82   &  25 &   5  &  0.146\\
    6  &  random &  7.89   &  25 &   3  &  0.163\\
    7  &  random &  8.76   &  25 &   8  &  0.176\\
    8  &  random &  9.07   &  25 &   6  &  0.183\\
    9  &  random &  91.61  &  25 &   4  &  0.204\\
    10 &  random &  92.75  &  25 &   10 &  0.107\\
    11 &  random &  92.75  &  25 &   6  &  0.108\\
    12 &  random &  92.87  &  25 &   9  &  0.114\\
    13 &  random &  9.44   &  25 &   3  &  0.188\\
    14 &  random &  94.95  &  25 &   7  &  0.163\\
    15 &  random &  9.54   &  25 &   10 &  0.179\\
    16 &  random &  96.49  &  25 &   6  &  0.127\\
    17 &  random &  96.97  &  25 &   5  &  0.21 \\
    18 &  random &  99.00  &  25 &   5  &  0.101\\
    19 &  random &  9.95   &  25 &   4  &  0.203\\
    20 &  random &  9.99   &  25 &   10 &  0.289\\
    \hline
  \end{tabular}
  \label{tab:10-networks}
\end{table}


\begin{table}[p!]
  \caption{Properties of network models of size N=45. $\kappa(\bA)$ is the
    interampatteness degree of the network, $\#$ SC is the number of
    strongly connected components in the network as reported by {\tt
      graphconncomp} in MATLAB. $\tau(\bG)$ is the time constant for
    the system $\mG = -\mA^{-1}$.}
  \centering
  \begin{tabular}{rlrrrr}
    ID & Type & \(\kappa(\bA)\) & $\#$ links & $\#$ SC & \(\tau(\bG)\)\\
    \hline
    1  & random & 25.42  & 145.00 & 10 & 0.144 \\
    2  & random & 27.16  & 144.00 & 25 & 0.104 \\
    3  & random & 28.15  & 141.00 & 11 & 0.103 \\
    4  & random & 28.72  & 146.00 & 10 & 0.139 \\
    5  & random & 30.52  & 141.00 & 16 & 0.121 \\
    6  & random & 31.82  & 142.00 & 13 & 0.12  \\
    7  & random & 33.82  & 144.00 & 11 & 0.123 \\
    8  & random & 34.40  & 143.00 & 13 & 0.149 \\
    9  & random & 40.47  & 144.00 & 14 & 0.114 \\
    10 & random & 411.53 & 146.00 & 12 & 0.159 \\
    11 & random & 41.31  & 141.00 & 11 & 0.105 \\
    12 & random & 428.70 & 144.00 & 8  & 0.132 \\
    13 & random & 434.30 & 145.00 & 7  & 0.1   \\
    14 & random & 452.73 & 140.00 & 15 & 0.118 \\
    15 & random & 458.01 & 142.00 & 16 & 0.1   \\
    16 & random & 460.35 & 142.00 & 10 & 0.102 \\
    17 & random & 482.32 & 144.00 & 12 & 0.11  \\
    18 & random & 484.46 & 143.00 & 14 & 0.123 \\
    19 & random & 489.59 & 142.00 & 13 & 0.101 \\
    20 & random & 492.81 & 145.00 & 7  & 0.1   \\
    \hline
  \end{tabular}
  \label{tab:45-networks}
\end{table}


\begin{table}[p!]
  \caption{Properties of SSSD data sets used
    in simulations. $\kappa(\mA)$ is the
    interampatteness degree of the network and $\kappa(\mY)$ is the
    condition number of the response matrix. $\# (~p_{ij} \neq 0)$ is
    the total number of perturbations in the matrix $\mP$. The
    $\overline{\bp_{1,0}} = \max_j|\bp_{i} \neq 0|_1$, that is, the maximum number of perturbations/experiment over all experiments and $\underline{\bp_{1,0}} = \min_j||\bp_{i}
    \neq 0||_1$ is the corresponding minimum. $\lambda(\SNR = 1)$ is the variance of the noise
    for $\SNR=1$.}
  \centering
    \begin{tabular}{rrrrrrr}
      ID & $\kappa(\bA)$ & $\kappa(\bY)$ & $\# p_{ij}$ & $\min_i\sum_j p_{ij}$ & $\max_i\sum_j p_{ij}$ & $\lambda(SNR = 1)$\\
      \hline
      1  & 25.42  & 25.67  & 225 & 1 & 2 & 6.37e-05 \\
      2  & 27.16  & 27.26  & 225 & 1 & 2 & 5.61e-05 \\
      3  & 28.15  & 28.38  & 225 & 1 & 2 & 5.17e-05 \\
      4  & 28.72  & 28.76  & 225 & 1 & 2 & 5.06e-05 \\
      5  & 30.52  & 30.59  & 225 & 1 & 2 & 4.42e-05 \\
      6  & 31.82  & 32.50  & 225 & 1 & 2 & 4.06e-05 \\
      7  & 33.82  & 34.16  & 225 & 1 & 2 & 3.62e-05 \\
      8  & 34.40  & 33.79  & 225 & 1 & 2 & 3.45e-05 \\
      9  & 40.47  & 41.55  & 225 & 1 & 2 & 2.43e-05 \\
      10 & 411.53 & 412.78 & 225 & 1 & 2 & 2.44e-07 \\
      11 & 41.31  & 41.86  & 225 & 1 & 2 & 2.38e-05 \\
      12 & 428.70 & 447.17 & 225 & 1 & 2 & 2.15e-07 \\
      13 & 434.30 & 439.99 & 225 & 1 & 2 & 2.15e-07 \\
      14 & 452.73 & 453.91 & 225 & 1 & 2 & 2.02e-07 \\
      15 & 458.01 & 459.05 & 225 & 1 & 2 & 1.97e-07 \\
      16 & 460.35 & 469.02 & 225 & 1 & 2 & 1.89e-07 \\
      17 & 482.32 & 488.93 & 225 & 1 & 2 & 1.74e-07 \\
      18 & 484.46 & 495.41 & 225 & 1 & 2 & 1.7e-07  \\
      19 & 489.59 & 495.54 & 225 & 1 & 2 & 1.71e-07 \\
      20 & 492.81 & 504.51 & 225 & 1 & 2 & 1.64e-07 \\
      \hline
    \end{tabular}
  \label{tab:45-datasets}
\end{table}

\clearpage


\subsection{Signed MCC}
We also wanted to take into account the sign of each link and
therefore we extended the MCC to the Signed MCC (SMCC) with the
conditions in \tabref{tab:SMCC}. Similar extensions to performance
measures have been used before to account for a signed structure
of the inferred model, see \eg \citet{Hache2009}. Here we define a
false negative (FN) as a link that is wrongly assumed to not exist
or has the wrong sign, \ie our hypothesis is wrong when we should
have a link with a specific sign.  The optimal estimate is then
obtained for the regularisation parameter $\zeta$ that maximises
SMCC.

\begin{table}[h!]
  \caption{Signed Matthew Correlation Coefficient (SMCC) extension to MCC}
  \centering
  \begin{tabular}{rr|lll}
    & & \multicolumn{3}{c}{Inferred}\\
    \cline{3-5}
     & & 1 & 0  & -1 \\
    \hline
    \multicolumn{1}{c|}{\multirow{3}{*}{\rotatebox{90}{True}}} & 1 & TP & FN & FN \\
    \multicolumn{1}{c|}{} & 0 & FP & TN & FP \\
    \multicolumn{1}{c|}{} & -1 & FN & FN & TP \\
  \end{tabular}
  \label{tab:SMCC}
\end{table}

\begin{figure}[p!]
  \centering
  \subfigure[Data set with $\kappa(A)=100.56$ and $\kappa(Y)=93.69$ corresponding to ID$=1$ in \tabref{tab:NRDP-datasets}]
  {\label{fig:lassofail100}
    \includegraphics[width=0.5\linewidth]{./img/1005621_absolute_kY936945_SMCC}}
  \subfigure[Data set with $\kappa(A)=92.75$ and $\kappa(Y)=124.70$ corresponding to ID$=10$ in \tabref{tab:NRDP-datasets}]
  {\label{fig:lassofail92}
    \includegraphics[width=0.5\linewidth]{./img/927523_absolute_kY1247001_SMCC}}
  \subfigure[Data set with $\kappa(A)=6.90$ and $\kappa(Y)=9.52$ corresponding to ID$=3$ in \tabref{tab:NRDP-datasets}]
  {\label{fig:lassofail6}
    \includegraphics[width=0.5\linewidth]{./img/68983_absolute_kY95167_SMCC}}
  \caption{Optimal network inference with corresponding link estimates for
    \lasso (blue) and LSCO (green) compared to true network (red),
    evaluated at a variance, $\lambda = 10^{-9}$. True Links are
    marked with sign and denoted with $(i,j)$ where $j$ influences
    $i$. False links are only marked by the influence of the link
    \ie the sign marks the true network influence, $\{+,-,0\}$.}
  \label{fig:lassoFail}
\end{figure}


\begin{figure}[p!]
  \centering
  \includegraphics[width=0.5\linewidth]{./img/performanceHighkYSNR2}
  \caption{GRN inference accuracy versus signal to noise ratio using
    \lasso, LSCO, and RNI on NRDP data sets with high condition number
    $\kappa(\bY)$. \lasso fails even when all existing
    links can be proven to exist, \ie when RNI reaches FPEL$=1$.
    Boxes are grouped according to five SNR values. Box edges
    signifies $q_1=25$th and $q_3=75$th percentile, whiskers
    encapsulate the most extreme data points not considered
    outliers. Outliers are considered points which are $>q_3 + w(q_3 -
    q_1)$ or $<q_1 - w(q_3 - q_1)$ where $w=1.5$ and marked with
    {+}.}
  \label{fig:highkYopt-sign}
\end{figure}


\begin{figure}[p!]
  \centering
  \includegraphics[width=0.5\linewidth]{./img/performanceLowkYSNR2}
  \caption{GRN inference accuracy versus signal to noise ratio using
    \lasso, LSCO, and RNI on SBED data sets with low condition number
    $\kappa(\bY)$.
    For an SNR of $10$ both \lasso and LSCO can infer the
    true network structure for some of the data sets even though all existing
    links cannot be proven to exist (RNI has a FPEL $<1$). For an
    SNR $>10$ the median of all methods inference accuracy is
    approaching 1 and is above 90\% for all data sets. For a description
    of the plot see \figref{fig:highkYopt}.}
  \label{fig:lowkYopt-sign}
\end{figure}


%%% Local Variables:
%%% TeX-master: "main-manuscript"
%%% ispell-local-dictionary: "en_GB"
%%% End:
